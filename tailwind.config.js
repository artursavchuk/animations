/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: '#ffee58',
        secondary: '#BC1E22',
      },
    },
  },
  plugins: [],
}
