import { useFrame, useThree } from '@react-three/fiber'
import {
  easeOut,
  useMotionValue,
  useMotionValueEvent,
  useScroll,
  useTransform,
} from 'framer-motion'
import { hero, about, info } from '../helpers/vectors'

export const useVectors = ({ heroProgress, aboutProgress, infoProgress }) => {
  useThree((state) => state.viewport) // to update getters in helpers/vectors
  const { scrollY } = useScroll()
  
  // current section
  const sectionName = useMotionValue('hero')
  useMotionValueEvent(heroProgress, 'change', () => sectionName.set('hero'))
  useMotionValueEvent(aboutProgress, 'change', () => sectionName.set('about'))
  useMotionValueEvent(infoProgress, 'change', () => sectionName.set('info'))

  // helper to calculate a computed value based on scroll position
  const useComputed = (progress, axisValues) => {
    if (axisValues.length === 1) {
      axisValues = [axisValues[0], axisValues[0]]
    }

    const scrollPoints = Array.from(
      axisValues,
      (_, i) => i / (axisValues.length - 1)
    )

    return useTransform(progress, scrollPoints, axisValues, { ease: easeOut })
  }

  // computed values of ship and camera
  const vectors = {
    hero: {
      ship: {
        position: {
          x: useComputed(heroProgress, hero.ship.position.x),
          y: useComputed(heroProgress, hero.ship.position.y),
          z: useComputed(heroProgress, hero.ship.position.z),
        },
        rotation: {
          x: useComputed(heroProgress, hero.ship.rotation.x),
          y: useComputed(heroProgress, hero.ship.rotation.y),
          z: useComputed(heroProgress, hero.ship.rotation.z),
        },
      },
      camera: {
        position: {
          x: useComputed(heroProgress, hero.camera.position.x),
          y: useComputed(heroProgress, hero.camera.position.y),
          z: useComputed(heroProgress, hero.camera.position.z),
        },
        lookAt: {
          x: useComputed(heroProgress, hero.camera.lookAt.x),
          y: useComputed(heroProgress, hero.camera.lookAt.y),
          z: useComputed(heroProgress, hero.camera.lookAt.z),
        },
      },
    },
    about: {
      ship: {
        position: {
          x: useComputed(aboutProgress, about.ship.position.x),
          y: useComputed(aboutProgress, about.ship.position.y),
          z: useComputed(aboutProgress, about.ship.position.z),
        },
        rotation: {
          x: useComputed(aboutProgress, about.ship.rotation.x),
          y: useComputed(aboutProgress, about.ship.rotation.y),
          z: useComputed(aboutProgress, about.ship.rotation.z),
        },
      },
      camera: {
        position: {
          x: useComputed(aboutProgress, about.camera.position.x),
          y: useComputed(aboutProgress, about.camera.position.y),
          z: useComputed(aboutProgress, about.camera.position.z),
        },
        lookAt: {
          x: useComputed(aboutProgress, about.camera.lookAt.x),
          y: useComputed(aboutProgress, about.camera.lookAt.y),
          z: useComputed(aboutProgress, about.camera.lookAt.z),
        },
      },
    },
    info: {
      ship: {
        position: {
          x: useComputed(infoProgress, info.ship.position.x),
          y: useComputed(infoProgress, info.ship.position.y),
          z: useComputed(infoProgress, info.ship.position.z),
        },
        rotation: {
          x: useComputed(infoProgress, info.ship.rotation.x),
          y: useComputed(infoProgress, info.ship.rotation.y),
          z: useComputed(infoProgress, info.ship.rotation.z),
        },
      },
      camera: {
        position: {
          x: useComputed(infoProgress, info.camera.position.x),
          y: useComputed(infoProgress, info.camera.position.y),
          z: useComputed(infoProgress, info.camera.position.z),
        },
        lookAt: {
          x: useComputed(infoProgress, info.camera.lookAt.x),
          y: useComputed(infoProgress, info.camera.lookAt.y),
          z: useComputed(infoProgress, info.camera.lookAt.z),
        },
      },
    },
  }

  // updating motion vales for returning reactive values to component
  // ship position
  const shipPositionX = useTransform([scrollY, sectionName], ([_, n]) =>
    vectors[n].ship.position.x.get()
  )
  const shipPositionY = useTransform([scrollY, sectionName], ([_, n]) =>
    vectors[n].ship.position.y.get()
  )
  const shipPositionZ = useTransform([scrollY, sectionName], ([_, n]) =>
    vectors[n].ship.position.z.get()
  )
  // ship rotation
  const shipRotationX = useTransform([scrollY, sectionName], ([_, n]) =>
    vectors[n].ship.rotation.x.get()
  )
  const shipRotationY = useTransform([scrollY, sectionName], ([_, n]) =>
    vectors[n].ship.rotation.y.get()
  )
  const shipRotationZ = useTransform([scrollY, sectionName], ([_, n]) =>
    vectors[n].ship.rotation.z.get()
  )
  // camera position
  const cameraPositionX = useTransform([scrollY, sectionName], ([_, n]) =>
    vectors[n].camera.position.x.get()
  )
  const cameraPositionY = useTransform([scrollY, sectionName], ([_, n]) =>
    vectors[n].camera.position.y.get()
  )
  const cameraPositionZ = useTransform([scrollY, sectionName], ([_, n]) =>
    vectors[n].camera.position.z.get()
  )
  // camera lookAt
  const cameraLookAtX = useTransform([scrollY, sectionName], ([_, n]) =>
    vectors[n].camera.lookAt.x.get()
  )
  const cameraLookAtY = useTransform([scrollY, sectionName], ([_, n]) =>
    vectors[n].camera.lookAt.y.get()
  )
  const cameraLookAtZ = useTransform([scrollY, sectionName], ([_, n]) =>
    vectors[n].camera.lookAt.z.get()
  )

  // updating camera
  useFrame((state) => {
    const { camera } = vectors[sectionName.get()]

    state.camera.position.x = camera.position.x.get()
    state.camera.position.y = camera.position.y.get() + 2
    state.camera.position.z = camera.position.z.get()

    state.camera.lookAt(
      camera.lookAt.x.get(),
      camera.lookAt.y.get(),
      camera.lookAt.z.get()
    )
  })

  return {
    ship: {
      position: {
        x: shipPositionX,
        y: shipPositionY,
        z: shipPositionZ,
      },
      rotation: {
        x: shipRotationX,
        y: shipRotationY,
        z: shipRotationZ,
      },
    },
    camera: {
      position: {
        x: cameraPositionX,
        y: cameraPositionY,
        z: cameraPositionZ,
      },
      lookAt: {
        x: cameraLookAtX,
        y: cameraLookAtY,
        z: cameraLookAtZ,
      },
    },
  }
}
