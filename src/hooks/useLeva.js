import { useMotionValueEvent } from 'framer-motion'
import { useControls } from 'leva'

export const useLeva = ({ ship, camera }) => {
  const [_, setShipPosition] = useControls('shipPosition', () => ({
    x: {
      value: ship.position.x.get(),
      min: -10,
      max: 10,
      onChange: (v) => ship.position.x.set(v),
    },
    y: {
      value: ship.position.y.get(),
      min: -10,
      max: 10,
      onChange: (v) => ship.position.y.set(v),
    },
    z: {
      value: ship.position.z.get(),
      min: -10,
      max: 10,
      onChange: (v) => ship.position.z.set(v),
    },
  }))

  const [_2, setShipRotation] = useControls('shipRotation', () => ({
    x: {
      value: ship.rotation.x.get(),
      min: -10,
      max: 10,
      onChange: (v) => ship.rotation.x.set(v),
    },
    y: {
      value: ship.rotation.y.get(),
      min: -10,
      max: 10,
      onChange: (v) => ship.rotation.y.set(v),
    },
    z: {
      value: ship.rotation.z.get(),
      min: -10,
      max: 10,
      onChange: (v) => ship.rotation.z.set(v),
    },
  }))

  const [_3, setCameraPosition] = useControls('cameraPosition', () => ({
    x: {
      value: camera.position.x.get(),
      min: -10,
      max: 10,
      onChange: (v) => camera.position.x.set(v),
    },
    y: {
      value: camera.position.y.get(),
      min: -10,
      max: 10,
      onChange: (v) => camera.position.y.set(v),
    },
    z: {
      value: camera.position.z.get(),
      min: -10,
      max: 10,
      onChange: (v) => camera.position.z.set(v),
    },
  }))

  const [_4, setCameraLookAt] = useControls('cameraLookAt', () => ({
    x: {
      value: camera.lookAt.x.get(),
      min: -10,
      max: 10,
      onChange: (v) => camera.lookAt.x.set(v),
    },
    y: {
      value: camera.lookAt.y.get(),
      min: -10,
      max: 10,
      onChange: (v) => camera.lookAt.y.set(v),
    },
    z: {
      value: camera.lookAt.z.get(),
      min: -10,
      max: 10,
      onChange: (v) => camera.lookAt.z.set(v),
    },
  }))

  // set ship position to controls
  useMotionValueEvent(ship.position.x, 'change', (latest) =>
    setShipPosition({ x: latest })
  )
  useMotionValueEvent(ship.position.y, 'change', (latest) =>
    setShipPosition({ y: latest })
  )
  useMotionValueEvent(ship.position.z, 'change', (latest) =>
    setShipPosition({ z: latest })
  )

  // set ship rotation to controls
  useMotionValueEvent(ship.rotation.x, 'change', (latest) =>
    setShipRotation({ x: latest })
  )
  useMotionValueEvent(ship.rotation.y, 'change', (latest) =>
    setShipRotation({ y: latest })
  )
  useMotionValueEvent(ship.rotation.z, 'change', (latest) =>
    setShipRotation({ z: latest })
  )

  // set camera position to controls
  useMotionValueEvent(camera.position.x, 'change', (latest) =>
    setCameraPosition({ x: latest })
  )
  useMotionValueEvent(camera.position.y, 'change', (latest) =>
    setCameraPosition({ y: latest })
  )
  useMotionValueEvent(camera.position.z, 'change', (latest) =>
    setCameraPosition({ z: latest })
  )

  // set camera lookAt to controls
  useMotionValueEvent(camera.lookAt.x, 'change', (latest) =>
    setCameraLookAt({ x: latest })
  )
  useMotionValueEvent(camera.lookAt.y, 'change', (latest) =>
    setCameraLookAt({ y: latest })
  )
  useMotionValueEvent(camera.lookAt.z, 'change', (latest) =>
    setCameraLookAt({ z: latest })
  )
}
