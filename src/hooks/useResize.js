import { useEffect, useState } from 'react'

export const useResize = () => {
  const [isMobile, setIsMobile] = useState(
    window.matchMedia('(max-width: 767px)').matches
  )

  useEffect(() => {
    const checkWindow = () => {
      setIsMobile(window.matchMedia('(max-width: 767px)').matches)
    }
    window.addEventListener('resize', checkWindow)
    return () => {
      window.removeEventListener('resize', checkWindow)
    }
  }, [])

  return { isMobile }
}
