import { Canvas } from '@react-three/fiber'
import { useScroll } from 'framer-motion'
import { useEffect, useRef, useState } from 'react'
import { CanvasContent } from './components/CanvasContent'
import { Header } from './sections/Header'
import { Hero } from './sections/hero'
import { About } from './sections/About'
import { Info } from './sections/Info'
import { Ships } from './sections/Ships'
import { Characters } from './sections/Characters'
import { WithCustomCursor } from './components/WithCustomCursor'

export const App = () => {
  const [loading, setLoading] = useState(true)
  const hero = useRef(null)
  const info = useRef(null)

  useEffect(() => {
    const handleLoad = () => {
      window.scrollTo(0, 0)
      setTimeout(() => setLoading(false), 500)
    }
    window.addEventListener('load', handleLoad)
    return () => {
      window.removeEventListener('load', handleLoad)
    }
  }, [])

  const { scrollYProgress: heroProgress } = useScroll({
    target: hero,
    offset: ['end 90%', 'end 0%'],
    smooth: true,
  })
  const { scrollYProgress: aboutProgress } = useScroll({
    target: info,
    offset: ['start 100%', 'end 100%'],
  })
  const { scrollYProgress: infoProgress } = useScroll({
    target: info,
    offset: ['end 100%', 'end start'],
  })

  return (
    <WithCustomCursor>
      <div className="scroll-container">
        <Canvas
          style={{
            width: '100vw',
            height: '100dvh',
            position: 'fixed',
            inset: 0,
            pointerEvents: 'none',
          }}
          camera={{ position: [0, 2, 3.5] }}
        >
          <CanvasContent
            heroProgress={heroProgress}
            aboutProgress={aboutProgress}
            infoProgress={infoProgress}
          />
        </Canvas>

        {loading && <div className="preloader">loading</div>}

        <Header />
        <main className="flex flex-col gap-20 relative z-10">
          <Hero ref={hero} />
          <About />
          <Info ref={info} />
          <Ships />
          <Characters />
        </main>
      </div>
    </WithCustomCursor>
  )
}
