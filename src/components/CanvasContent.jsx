import { Environment, Float, SpotLight } from '@react-three/drei'
import { XWing } from './XWing'
import { useRef } from 'react'
import { motion as motion3d } from 'framer-motion-3d'
import { useVectors } from '../hooks/useVectors'
import { Stars } from './Stars'
import { useLeva } from '../hooks/useLeva'
import { useTransform } from 'framer-motion'

export const CanvasContent = (props) => {
  const { heroProgress, aboutProgress, infoProgress } = props
  const shipRef = useRef(null)

  const { ship, camera } = useVectors({
    heroProgress,
    aboutProgress,
    infoProgress,
  })
  const initialScale = window.innerWidth < 600 ? 0.15 : 0.3
  const scale = useTransform(infoProgress, [0.6, 1], [initialScale, 0])
  // useLeva({ ship, camera })

  return (
    <mesh>
      <motion3d.mesh
        ref={shipRef}
        position={[ship.position.x, ship.position.y, ship.position.z]}
        rotation={[ship.rotation.x, ship.rotation.y, ship.rotation.z]}
        scale={scale}
      >
        <Float floatIntensity={10}>
          <XWing castShadow />
        </Float>
      </motion3d.mesh>

      <Environment preset="city" />
      <Stars scale={5} />
      <Float floatIntensity={2}>
        <SpotLight
          position={[-4, 3, 1.2]}
          distance={20}
          angle={1.3}
          attenuation={10}
          anglePower={50}
          color={'red'}
        />
      </Float>
    </mesh>
  )
}
