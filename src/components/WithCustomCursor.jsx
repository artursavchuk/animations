import { createContext, useEffect, useState } from 'react'
import { motion, useMotionValue, useSpring } from 'framer-motion'

export const CursorContext = createContext(null)

export const WithCustomCursor = ({ children }) => {
  const [isHovered, setIsHovered] = useState(false)
  const [sizeScaling, setSizeScaling] = useState(1)

  const size = sizeScaling * (isHovered ? 150 : 30)

  const position = {
    x: useMotionValue(0),
    y: useMotionValue(0),
  }
  const smoothOptions = { damping: 30, stiffness: 300, mass: 0.1 }
  const smooth = {
    x: useSpring(position.x, smoothOptions),
    y: useSpring(position.y, smoothOptions),
  }
  const opacity = useMotionValue(1)

  useEffect(() => {
    const handleMove = (e) => {
      position.x.set(e.clientX - size / 2)
      position.y.set(e.clientY - size / 2)
      opacity.set(1)
    }
    const handleOut = () => {
      opacity.set(0)
    }
    window.addEventListener('mousemove', handleMove)
    document.addEventListener('mouseout', handleOut)
    return () => {
      window.removeEventListener('mousemove', handleMove)
      document.removeEventListener('mouseout', handleOut)
    }
  }, [isHovered])

  return (
    <CursorContext.Provider
      value={{
        isHovered,
        setIsHovered,
        setSizeScaling,
      }}
    >
      <>
        <motion.div
          className="w-[30px] h-[30px] bg-white fixed top-0 left-0 z-[99] pointer-events-none rounded-full mix-blend-difference"
          style={{
            left: smooth.x,
            top: smooth.y,
            opacity: opacity,
          }}
          animate={{
            width: `${size}px`,
            height: `${size}px`,
          }}
          transition={{
            type: 'tween',
            duration: 0.2,
          }}
        />
        {children}
      </>
    </CursorContext.Provider>
  )
}
