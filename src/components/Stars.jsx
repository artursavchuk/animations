import { PointMaterial, Points } from '@react-three/drei'
import { useFrame } from '@react-three/fiber'
import { useRef, useState } from 'react'
import * as random from 'maath/random/dist/maath-random.esm'

export const Stars = (props) => {
  const ref = useRef()
  const [sphere] = useState(() =>
    random.inSphere(new Float32Array(10000), { radius: 1.5 })
  )
  useFrame((state, delta) => {
    ref.current.rotation.x -= delta * 0.002
    ref.current.rotation.y -= delta * 0.003
  })
  return (
    <group rotation={[0, 0, Math.PI / 4]}>
      <Points
        ref={ref}
        positions={sphere}
        stride={3}
        frustumCulled={false}
        {...props}
      >
        <PointMaterial
          transparent
          color="#ffa0e0"
          size={0.0075}
          sizeAttenuation={true}
          depthWrite={false}
        />
      </Points>
    </group>
  )
}
