import { AnimatePresence, motion } from 'framer-motion'
import { StarWarsText } from './StarWarsText'
import { CursorSettings } from './CursorSettings'

export const Card = ({ card, setSelected }) => {
  const { id, title, image } = card ?? {}

  return (
    <>
      <motion.div
        layoutId={'card' + id}
        className="card"
        onClick={() => setSelected(card)}
        transition={{ duration: 0.35 }}
      >
        <div className="card-content">
          <motion.img
            layoutId={'image' + id}
            src={image}
            alt={title}
            className="max-w-[60%] m-auto"
          />
          <h3 className="text-center">{title}</h3>
        </div>
      </motion.div>
      <motion.div
        layoutId={'backdrop' + id}
        className="fixed left-1/2 top-1/2 [translate:-50%_-50%] w-[100vw] h-[100vh] pointer-events-none"
        transition={{ duration: 0.35 }}
      />
    </>
  )
}

export const SelectedCard = ({ card, setSelected }) => {
  const { id, title, description, image } = card ?? {}

  return (
    <AnimatePresence>
      {card && (
        <>
          <CursorSettings scaling={0}>
            <motion.div
              layoutId={'backdrop' + id}
              className="fixed left-1/2 top-1/2 [translate:-50%_-50%] w-[100vw] h-[100vh] bg-[rgba(256,256,256,0.15)] z-10"
              onClick={() => setSelected(null)}
              transition={{ duration: 0.35 }}
            />

            <motion.div
              layoutId={'card' + id}
              className="fixed left-1/2 top-1/2 [translate:-50%_-50%] w-[calc(100%-30px)] sm:w-[500px] z-30 flex-grow"
              onClick={(e) => e.stopPropagation()}
              transition={{ duration: 0.35 }}
            >
              <motion.div
                transition={{ delay: 0.15, duration: 0.3 }}
                initial={{ opacity: 0.3 }}
                animate={{ opacity: 1 }}
                className="bg-secondary absolute left-1/2 -translate-x-1/2 md:translate-x-0 md:-left-[175px] -top-[30%] md:top-1/2 md:-translate-y-1/2 -z-10 flex h-[300px] md:-rotate-90 rounded-xl"
              >
                <StarWarsText className="fill-[rgba(0,0,0,0.2)] max-w-full h-[200px]" />
              </motion.div>

              <div className="bg-[black] relative py-8 px-4 grid md:grid-cols-[150px_1fr] rounded-xl">
                <motion.img
                  layoutId={'image' + id}
                  src={image}
                  alt={title}
                  className="max-w-[225px] m-auto z-20 relative md:-left-2/3"
                />

                <div>
                  <button
                    className="absolute top-2 right-5"
                    onClick={() => setSelected(null)}
                  >
                    x
                  </button>
                  <h3 className="mb-4">{title}</h3>
                  <p>{description}</p>
                </div>
              </div>
            </motion.div>
          </CursorSettings>
        </>
      )}
    </AnimatePresence>
  )
}
