import { useContext } from 'react'
import { CursorContext } from './WithCustomCursor'

export const CursorSettings = ({ children, scaling }) => {
  const { setIsHovered, setSizeScaling } = useContext(CursorContext)

  const handleEnter = () => {
    setIsHovered(true)
    if (scaling || scaling === 0) setSizeScaling(scaling)
  }
  const handleLeave = () => {
    setIsHovered(false)
    if (scaling || scaling === 0) setSizeScaling(1)
  }

  return (
    <div onMouseEnter={handleEnter} onMouseLeave={handleLeave}>
      {children}
    </div>
  )
}
