// tailwind breakpoints
// 'sm' => '640px',
// 'md' => '768px',
// 'lg' => '1024px',
// 'xl' => '1280px',

export const hero = {
  ship: {
    position: {
      get x() {
        if (window.matchMedia('(max-width: 400px)').matches) {
          return [0.8, -3.5]
        }
        if (window.matchMedia('(max-width: 639px)').matches) {
          return [1.2, -4]
        }
        if (window.matchMedia('(max-width: 767px)').matches) {
          return [1.4, -4.5]
        }
        if (window.matchMedia('(max-width: 1023px)').matches) {
          return [2, -4.8]
        }

        return [2.6, -5.2]
      },
      get y() {
        if (window.matchMedia('(max-width: 400px)').matches) {
          return [-5, -1.7]
        }
        if (window.matchMedia('(max-width: 639px)').matches) {
          return [-3.1, -1.7]
        }
        return [-0.9, -1.7]
      },
      z: [0.3, 0.2],
    },
    rotation: {
      x: [-0.7, -0.9],
      y: [-0.8, 0.6],
      z: [0, 0],
    },
  },
  camera: {
    position: {
      x: [-0.5, -2.5],
      y: [1.5, 1.2],
      z: [3.4, 2.1],
    },
    lookAt: {
      x: [-0.3, -2.6],
      y: [0, -0.4],
      z: [0, 0.5],
    },
  },
}

export const about = {
  ship: {
    position: {
      get x() {
        const prev = hero.ship.position.x.at(-1)
        if (window.matchMedia('(max-width: 639px)').matches) {
          return [prev, -1]
        }
        if (window.matchMedia('(max-width: 767px)').matches) {
          return [prev, 0.1]
        }
        if (window.matchMedia('(max-width: 1023px)').matches) {
          return [prev, 0.4]
        }

        return [prev, 0.8]
      },
      y: [hero.ship.position.y.at(-1), -3.1],
      z: [hero.ship.position.z.at(-1), -0.8],
    },
    rotation: {
      x: [hero.ship.rotation.x.at(-1), -0.7],
      y: [hero.ship.rotation.y.at(-1), -1.1],
      z: [hero.ship.rotation.z.at(-1), 0.2],
    },
  },
  camera: {
    position: {
      x: [hero.camera.position.x.at(-1)],
      y: [hero.camera.position.y.at(-1)],
      z: [hero.camera.position.z.at(-1)],
    },
    lookAt: {
      x: [hero.camera.lookAt.x.at(-1)],
      y: [hero.camera.lookAt.y.at(-1)],
      z: [hero.camera.lookAt.z.at(-1)],
    },
  },
}

export const info = {
  ship: {
    position: {
      get x() {
        const prev = about.ship.position.x.at(-1)
        // if (window.matchMedia('(max-width: 639px)').matches) {
        //   return [prev, 0.4, -10]
        // }
        // if (window.matchMedia('(max-width: 767px)').matches) {
        //   return [prev, 0.4, -10]
        // }
        // if (window.matchMedia('(max-width: 1023px)').matches) {
        //   return [prev, 0.4, -10]
        // }

        return [prev, 0.4, -10]
      },
      y: [about.ship.position.y.at(-1), -7],
      z: [about.ship.position.z.at(-1), 2.6],
    },
    rotation: {
      x: [about.ship.rotation.x.at(-1), -1],
      y: [about.ship.rotation.y.at(-1), -1.1],
      z: [about.ship.rotation.z.at(-1), 0],
    },
  },
  camera: {
    position: {
      x: [about.camera.position.x.at(-1), -2.6],
      y: [about.camera.position.y.at(-1), 1.2],
      z: [about.camera.position.z.at(-1), 2.1],
    },
    lookAt: {
      x: [about.camera.lookAt.x.at(-1), -2.7],
      y: [about.camera.lookAt.y.at(-1), -4.1],
      z: [about.camera.lookAt.z.at(-1), -0.2],
    },
  },
}
