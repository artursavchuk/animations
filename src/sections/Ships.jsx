import { forwardRef, useEffect, useState } from 'react'
import { LayoutGroup } from 'framer-motion'
import { Card, SelectedCard } from '../components/Card'
import imperial_shuttle from '/src/assets/imperial_shuttle.png'
import jedi_star_fighter from '/src/assets/jedi_star_fighter.png'
import naboo_fighter from '/src/assets/naboo_fighter.png'
import { CursorSettings } from '../components/CursorSettings'

const cardsData = [
  {
    image: imperial_shuttle,
    id: 'card-1',
    title: 'Imperial Shuttle',
    description:
      'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam labore corrupti facilis fugiat sit ipsa incidunt eligendi eveniet ut delectus id odio qui, non error. In delectus omnis deleniti cumque.',
  },
  {
    image: jedi_star_fighter,
    id: 'card-2',
    title: 'Jedi Star Fighter',
    description:
      'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam labore corrupti facilis fugiat sit ipsa incidunt eligendi eveniet ut delectus id odio qui, non error. In delectus omnis deleniti cumque. ',
  },
  {
    image: naboo_fighter,
    id: 'card-3',
    title: 'Naboo Fighter',
    description:
      'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam labore corrupti facilis fugiat sit ipsa incidunt eligendi eveniet ut delectus id odio qui, non error. In delectus omnis deleniti cumque.',
  },
]

const body = document.querySelector('body')

export const Ships = forwardRef((props, ref) => {
  const [selected, setSelected] = useState(null)

  useEffect(() => {
    body.style.overflow = selected ? 'hidden' : 'initial'
  }, [selected])

  const handleMouseMove = (e) => {
    for (const card of document.querySelectorAll('.card')) {
      const rect = card.getBoundingClientRect(),
        x = e.clientX - rect.left,
        y = e.clientY - rect.top

      card.style.setProperty('--mouse-x', `${x}px`)
      card.style.setProperty('--mouse-y', `${y}px`)
    }
  }
  return (
    <section
      id="ships"
      ref={ref}
      className="min-h-[100vh] flex flex-col mt-[40vh] md:mt-[75vh] scroll-mt-[15dvh]"
    >
      <div className="container flex flex-col flex-grow">
        <CursorSettings scaling={0.5}>
          <h2 className=" font-semibold text-4xl mb-6 block">Ships</h2>
        </CursorSettings>

        <CursorSettings scaling={0}>
          <LayoutGroup>
            <div
              className="cards flex flex-wrap gap-8"
              onMouseMove={handleMouseMove}
            >
              {cardsData.map((card) => (
                <Card key={card.id} card={card} setSelected={setSelected} />
              ))}
            </div>
            <SelectedCard card={selected} setSelected={setSelected} />
          </LayoutGroup>
        </CursorSettings>
      </div>
    </section>
  )
})
