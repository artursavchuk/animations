import { forwardRef } from 'react'
import { CursorSettings } from '../components/CursorSettings'

export const Hero = forwardRef((props, ref) => {
  return (
    <section id="hero" ref={ref} className="min-h-[100vh] flex flex-col">
      <div className="container flex-grow flex sm:items-center ">
        <div className="sm:max-w-[50%] max-w-[100%] pt-[15dvh] sm:pt-0">
          <CursorSettings>
            <h1 className="font-semibold text-4xl mb-10">
              Lorem ipsum dolor sit.
            </h1>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia
              obcaecati ipsa natus eius dolorem iure deleniti quisquam et.
            </p>
          </CursorSettings>
        </div>
      </div>
    </section>
  )
})
