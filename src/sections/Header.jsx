import { motion } from 'framer-motion'
import { useState } from 'react'
import { useResize } from '../hooks/useResize'
import { CursorSettings } from '../components/CursorSettings'

export const Header = () => {
  const { isMobile } = useResize()
  const [isOpened, setIsOpened] = useState(false)

  const handleClick = (selector) => {
    const section = document.querySelector(selector)
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' })
    }
    setIsOpened(false)
  }

  const variants = {
    open: isMobile ? { opacity: 1, x: 0 } : {},
    closed: isMobile ? { opacity: 0, x: '100%' } : {},
  }

  return (
    <header className="sticky top-0 left-0 w-100 min-h-[60px] flex items-center z-50">
      <div className="container flex gap-6">
        <CursorSettings scaling={0.5}>
          <div
            onClick={() => handleClick('#hero')}
            className="uppercase cursor-pointer"
          >
            LOGO
          </div>
        </CursorSettings>
        <motion.nav
          className={`${
            isMobile
              ? 'fixed inset-0 bg-black w-full h-[100dvh] py-[20dvh] px-10'
              : 'ml-auto'
          }`}
          animate={isOpened ? 'open' : 'closed'}
          variants={variants}
          transition={{ duration: 0.3 }}
        >
          <CursorSettings scaling={0.5}>
            <ul className="flex gap-8 flex-col sm:flex-row items-center">
              <li
                className="cursor-pointer"
                onClick={() => handleClick('#about')}
              >
                About
              </li>
              <li
                className="cursor-pointer"
                onClick={() => handleClick('#info')}
              >
                Info
              </li>
              <li
                className="cursor-pointer"
                onClick={() => handleClick('#ships')}
              >
                Ships
              </li>
              <li
                className="cursor-pointer"
                onClick={() => handleClick('#characters')}
              >
                Characters
              </li>
            </ul>
          </CursorSettings>
        </motion.nav>

        <div
          className="ml-auto block sm:hidden z-10"
          onClick={() => setIsOpened((isOpened) => !isOpened)}
        >
          <CursorSettings scaling={0.5}>
            {isOpened ? 'X' : 'MENU'}
          </CursorSettings>
        </div>
      </div>
    </header>
  )
}
