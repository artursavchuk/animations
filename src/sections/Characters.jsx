import c3po from '/src/assets/c-3po.png'
import sandman from '/src/assets/sandman.png'
import clone from '/src/assets/clone.png'
import dartvader from '/src/assets/dartvader.png'
import videoSandman from '/src/assets/video-sandman.webm'
import videoC3PO from '/src/assets/video-c3po.mp4'
import videoClone from '/src/assets/video-clone.webm'
import videoDartvader from '/src/assets/video-dartvader.mp4'
import { AnimatePresence, LayoutGroup, motion } from 'framer-motion'
import { useMemo, useState } from 'react'
import { useResize } from '../hooks/useResize'
import { CursorSettings } from '../components/CursorSettings'

const cards = [
  {
    id: 'c3po',
    title: 'C-3PO',
    image: c3po,
    video: videoC3PO,
    imageClasses: 'scale-[0.9] translate-y-[1%] translate-x-[5%]',
    description: `There's been some terrible mistake. I'm programmed for etiquette, not destruction!`,
  },
  {
    id: 'sandman',
    title: 'Tusken Raiders',
    image: sandman,
    video: videoSandman,
    imageClasses: 'scale-[0.95]  -translate-x-[4%]',
    description:
      'These people lay ancestral claim to the Dune Sea, and if you are to pass, a toll is to be paid to them',
  },
  {
    id: 'clone',
    title: 'Clone',
    image: clone,
    video: videoClone,
    imageClasses: 'scale-[0.9]',
    description: 'Good soldiers follow orders',
  },
  {
    id: 'dartvader',
    title: 'Dart Vader',
    image: dartvader,
    video: videoDartvader,
    imageClasses: 'scale-[1.75] translate-y-[45%] translate-x-[2%]',
    description:
      'A powerful Sith you will become. Henceforth, you shall be known as Darth… Vader',
  },
]

const initialState = cards.reduce((acc, card, i) => {
  return { ...acc, [card.id]: i === 0 ? card : null }
}, {})

export const Characters = () => {
  const { isMobile } = useResize()
  const [cardsState, setCardsState] = useState(initialState)

  const active = useMemo(
    () => Object.values(cardsState).find((c) => !!c),
    [cardsState]
  )

  const handleClick = (card) => {
    setCardsState((state) => {
      return Object.keys(state).reduce((acc, key) => {
        return { ...acc, [key]: key === card.id ? card : null }
      }, {})
    })
  }

  return (
    <section
      id="characters"
      className="min-h-[100vh] flex flex-col items-center relative -scroll-mt-[5dvh]"
    >
      <div className="w-full h-full overflow-hidden absolute inset-0 video-shadow">
        <video
          src={active.video}
          muted
          loop
          autoPlay
          playsInline
          className="absolute inset-0 h-full object-cover opacity-[0.15] scale-125"
        />
      </div>
      <div className="container flex flex-col flex-grow relative z-10">
        <div className="py-[15dvh]">
          <CursorSettings scaling={0.5}>
            <h2 className=" font-semibold text-4xl mb-8 md:mb-12 block">
              Characters
            </h2>
          </CursorSettings>
          <div className="flex flex-col md:grid md:grid-cols-[1fr,2fr] xl:grid-cols-2 md:gap-2 xl:gap-16">
            <LayoutGroup>
              {/* column */}
              <CursorSettings scaling={0}>
                <div className=" md:flex md:flex-col items-center gap-8 hidden">
                  {/* cards */}
                  <Circles
                    cards={cards}
                    cardsState={cardsState}
                    handleClick={handleClick}
                    isMobile={isMobile}
                  />
                </div>
              </CursorSettings>

              {/* column */}
              <div className="flex flex-col items-center gap-[5dvh] pt-[5dvh] relative">
                {/* tabs */}
                <CursorSettings scaling={0}>
                  <Tabs
                    cards={cards}
                    cardsState={cardsState}
                    handleClick={handleClick}
                  />
                </CursorSettings>

                {/* active card */}
                <CursorSettings scaling={0}>
                  <ActiveCard
                    cards={cards}
                    cardsState={cardsState}
                    handleClick={handleClick}
                    isMobile={isMobile}
                  />
                </CursorSettings>
              </div>
            </LayoutGroup>
          </div>
        </div>
      </div>
    </section>
  )
}

const Circles = ({ cards, cardsState, handleClick, isMobile }) => {
  return (
    <>
      {cards.map((card) => (
        <motion.div
          key={card.id}
          id={card.id}
          layoutId={isMobile ? null : card.id}
          className={`w-[min(25dvh,_250px)] rounded-xl cursor-pointer ${
            cardsState[card.id] ? 'hidden opacity-0' : ''
          }`}
          transition={{ duration: 0.4 }}
          onClick={() => handleClick(card)}
          animate={{ opacity: cardsState[card.id] ? 0 : 1 }}
        >
          <div className="rounded-full border-[4px] border-secondary w-[80%] aspect-square mx-auto bg-[rgba(188,30,34,0.2)]">
            <div className="h-[120%] -mt-[20%] rounded-[0_0_9999px_9999px] overflow-hidden">
              <motion.img
                src={card.image}
                alt={card.title}
                className={`object-contain ${card.imageClasses}`}
              />
            </div>
          </div>
          <h3 className="text-center text-secondary mt-2 hidden md:block">
            {card.title}
          </h3>
        </motion.div>
      ))}
    </>
  )
}

const Tabs = ({ cards, cardsState, handleClick }) => {
  return (
    <div className="flex gap-4 xl:gap-8 justify-center flex-wrap sm:flex-nowrap">
      {cards.map((tab) => (
        <button
          key={tab.id}
          onClick={() => handleClick(tab)}
          className={`${
            !cardsState[tab.id] &&
            'hover:border-white hover:bg-[rgba(256,256,256,0.15)]'
          } relative rounded-full px-5 py-1.5 text-sm font-medium text-white border-[1px] border-transparent transition duration-200 ease-linear`}
        >
          {tab.title}
          {cardsState[tab.id] && (
            <motion.span
              layoutId="bubble"
              className="absolute z-10 inset-0 bg-white mix-blend-difference rounded-full"
              transition={{ duration: 0.4 }}
            />
          )}
        </button>
      ))}
    </div>
  )
}

const ActiveCard = ({ cards, cardsState, handleClick, isMobile }) => {
  return (
    <div className="w-[calc(100%-30px)] md:w-[300px] grid relative">
      <AnimatePresence mode={isMobile ? 'wait' : 'popLayout'}>
        {Object.values(cardsState).map((card) => {
          if (!card) return null
          return (
            <motion.div
              key={card.id}
              layoutId={isMobile ? null : card.id}
              initial={{ y: isMobile ? '50px' : 0, opacity: 0 }}
              animate={{ y: 0, opacity: 1 }}
              exit={{ y: isMobile ? '50px' : 0, opacity: 0 }}
              className="w-full py-10 px-4 border-[4px] border-white rounded-xl flex md:flex-col gap-4 items-center bg-[rgba(256,256,256,0.15)] col-span-1 row-span-1 flex-col sm:flex-row"
            >
              <motion.img
                src={card.image}
                alt={card.title}
                className={`max-h-[30vh] object-contain min-w-[200px]`}
              />
              <div className="flex flex-col gap-4">
                <h3 className="md:text-center">{card.title}</h3>
                <p className="text-base">{card.description}</p>
              </div>
            </motion.div>
          )
        })}
      </AnimatePresence>
    </div>
  )
}
