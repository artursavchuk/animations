import { forwardRef } from 'react'
import { CursorSettings } from '../components/CursorSettings'

export const Info = forwardRef((props, ref) => {
  return (
    <section
      id="info"
      ref={ref}
      className="min-h-[150vh] flex flex-col mt-[10vh] scroll-mt-[5dvh]"
    >
      <div className="container flex-grow flex items-center">
        <div className="sm:max-w-[50%] max-w-[100%]">
          <CursorSettings>
            <h2 className="font-semibold text-4xl mb-6">
              Info section. Lorem ipsum dolor sit amet
            </h2>
            <p className="mb-3">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia
              obcaecati ipsa natus eius dolorem iure deleniti quisquam et, aut
              voluptatum.
            </p>
            <p className="mb-3">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis,
              adipisci magni expedita quae, illo quasi nam laudantium, harum
              exercitationem quam porro officiis dicta deserunt ut minus odit
              ea! Exercitationem nihil vitae molestias perferendis ipsa nulla
              eaque perspiciatis atque labore, assumenda eligendi nostrum
              voluptate in. Omnis quibusdam sunt molestias eveniet incidunt!
            </p>
            <p className="mb-3">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis,
              adipisci magni expedita quae, illo quasi nam laudantium, harum
              exercitationem quam porro officiis dicta deserunt ut minus odit
              ea! Exercitationem nihil vitae molestias perferendis ipsa nulla
              eaque perspiciatis atque labore, assumenda eligendi nostrum
              voluptate in. Omnis quibusdam sunt molestias eveniet incidunt!
            </p>
            <p className="mb-3">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis,
              adipisci magni expedita quae, illo quasi nam laudantium, harum
              exercitationem quam porro officiis dicta deserunt ut minus odit
              ea! Exercitationem nihil vitae molestias perferendis ipsa nulla
              eaque perspiciatis atque labore, assumenda eligendi nostrum
              voluptate in. Omnis quibusdam sunt molestias eveniet incidunt!
            </p>
            <p>
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ratione
              repellendus dicta adipisci, facilis quibusdam minus repudiandae
              quo provident sequi rerum saepe voluptate quia cupiditate dolor?
              Repellendus modi beatae maxime, qui quae ducimus sunt assumenda a
              eligendi, dolore earum voluptas omnis corrupti pariatur sit
              perspiciatis fugit porro dolorem quam neque! Sint.
            </p>
          </CursorSettings>
        </div>
      </div>
    </section>
  )
})
