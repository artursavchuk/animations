import { forwardRef } from 'react'
import { CursorSettings } from '../components/CursorSettings'

export const About = forwardRef((props, ref) => {
  return (
    <section
      id="about"
      ref={ref}
      className="min-h-[125vh] flex flex-col -scroll-mt-[5dvh]"
    >
      <div className="container flex flex-grow items-center">
        <div className="sm:max-w-[50%] max-w-[100%] sm:ml-auto">
          <CursorSettings>
            <h2 className="font-semibold text-4xl mb-6">About us</h2>
            <p className="mb-3">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci
              aut molestias rem laudantium voluptates dignissimos ut doloremque?
              Similique deleniti delectus unde, repellendus quis natus autem,
              necessitatibus numquam voluptatibus quas minus. Autem tempore
              recusandae ipsum, eum hic velit. Excepturi sequi accusamus nostrum
              modi ipsum placeat pariatur itaque nihil? Sunt, sapiente fuga.
            </p>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet,
              doloremque quo! Magnam architecto rerum delectus suscipit aliquam
              voluptate tempore! Culpa impedit harum necessitatibus, deleniti
              nesciunt nam velit unde mollitia alias.
            </p>
          </CursorSettings>
        </div>
      </div>
    </section>
  )
})
